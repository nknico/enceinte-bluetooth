# Enceinte Bluetooth

Bluetooth speaker with power bank function.



BOM :


- 2 x ultra-thin waterproof speaker 2W https://fr.aliexpress.com/item/32948199600.html?spm=a2g0s.9042311.0.0.1ec66c37CV5mgV

- 2 x passive radiators GHXAMP 65*40MM https://fr.aliexpress.com/item/32847332340.html?spm=a2g0s.9042311.0.0.1ec66c37j171Pq

- 1 x Amplifier Board PAM8403 https://www.aliexpress.com/item/4000500639517.html?spm=a2g0s.9042311.0.0.1ec66c37fKynZp

- 1 x protection board 3.7v - 5v https://www.aliexpress.com/item/32798858483.html?spm=a2g0s.9042311.0.0.1ec66c37fKynZp

- 1 x dual USB battery charger https://www.aliexpress.com/item/32805550547.html?spm=a2g0s.9042311.0.0.1ec66c37fKynZp

- 1 x simple power switch

- 8 x 18650 battery

- Nickel sheet plate

